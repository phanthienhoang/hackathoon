<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'title', 'avatar','content', 'date','time', 'idCategory','idUser'
    ];
}
