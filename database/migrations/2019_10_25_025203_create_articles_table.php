<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('avatar');
            $table->longtext('content');
            $table->date('date');
            $table->string('time');
            $table->unsignedBigInteger('idCategory');
            $table->unsignedBigInteger('idUser');
            $table->foreign('idCategory')
            ->references('id')
            ->on('categories')
            ->onDelete('cascade');
            $table->foreign('idUser')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
