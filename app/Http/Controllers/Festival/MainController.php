<?php

namespace App\Http\Controllers\Festival;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\Category;
use App\Model\Article;
use App\User;

class MainController extends Controller
{
    public function index(){
        return [
            'users'		=>User::all(),
            'categories'=>Category::all(),
            'articles'	=>Article::all(),
        ];
    }
}
