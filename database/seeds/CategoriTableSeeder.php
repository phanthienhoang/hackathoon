<?php

use Illuminate\Database\Seeder;
use App\Model\Category;

class CategoriTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = new Category();
        $category->title = 'TIN TỨC - SỰ KIỆN';
        $category->status = true;
        $category->save();


        $category = new Category();
        $category->title = 'CHƯƠNG TRÌNH, LỄ HỘI CHÍNH';
        $category->status = true;
        $category->save();

        $category = new Category();
        $category->title = 'ĐOÀN NGHỆ THUẬT & NGHỆ SĨ';
        $category->status = true;
        $category->save();
        
        $category = new Category();
        $category->title = 'NGHỀ VÀ CÁC LÀNG NGHỀ';
        $category->status = true;
        $category->save();
    }
}
