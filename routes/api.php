<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//hiển thị toàn bộ dữ liệu qua http://127.0.0.1:8000/api/
Route::get('/','Festival\MainController@index');

// category 

Route::group(['prefix'=>'category'], function(){
    Route::get('/','Festival\CategoryController@index');
    Route::get('/{category}','Festival\CategoryController@show');
    //link test post men http://127.0.0.1:8000/api/category/store
    Route::post('/store','Festival\CategoryController@store');
    Route::put('/update/{category}','Festival\CategoryController@update');
    Route::delete('/delete/{category}', 'Festival\CategoryController@destroy');
});
// article
Route::group(['prefix'=>'article'], function(){
    Route::get('/','Festival\ArticleController@index');
    Route::get('/{article}','Festival\ArticleController@show');
    //link test post men http://127.0.0.1:8000/api/article/store
    Route::post('/store','Festival\ArticleController@store');
    Route::put('/update/{article}','Festival\ArticleController@update');
    Route::delete('/delete/{article}', 'Festival\ArticleController@destroy');
});
